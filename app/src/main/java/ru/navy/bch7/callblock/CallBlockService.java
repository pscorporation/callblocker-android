package ru.navy.bch7.callblock;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.PhoneStateListener;
import android.util.Log;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;

public class CallBlockService extends Service {

    private static boolean startCommand = false;
    private static ITelephony iTelephony = null;
    public static final String TAG_CALLBLOCK = "callblock";
    private static AudioManager audioManager = null;
    private static CallBlockPhoneStateListener callBlockPhoneStateListener = null;

    Context mainContext;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public CallBlockService() {
        Log.d(TAG_CALLBLOCK, "boot init");
    }


    public static boolean IsCommandStarted()
    {
        return startCommand;
    }

    private static void answerPhoneHeadsethook(Context context)
    {
        Intent intent = new Intent(Intent.ACTION_MEDIA_BUTTON);


        intent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
        context.sendOrderedBroadcast(intent, "android.permission.CALL_PRIVILEGED");
        intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
        intent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
        context.sendOrderedBroadcast(intent, "android.permission.CALL_PRIVILEGED");

    }

    static void answerRingingCall(Context context)
    {
         try {
            iTelephony.answerRingingCall();
         } catch (RemoteException e) {
            e.printStackTrace();
         }

        //hSetCallResult = true;

        answerPhoneHeadsethook(context);
        return;
    }

    static void callEnd() {
        Log.d(TAG_CALLBLOCK, "terminating call");
        //iTelephony.endCall();
        try {
            iTelephony.endCall();
        }
        catch (Exception ex) {}



    }

    static AudioManager getAudioManager()
    {
        return audioManager;
    }

    public void onCreate()
    {
        mainContext = this;
    }

    public void onDestroy() {
        startCommand = false;
        iTelephony = null;
        audioManager = null;
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(CallBlockService.TAG_CALLBLOCK, "CallBlockService::onStartCommand");

        startCommand = true;

        callBlockPhoneStateListener = new CallBlockPhoneStateListener(this, this);

        ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE)).listen(callBlockPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);



        Log.d(CallBlockService.TAG_CALLBLOCK, "state2 =" +state);




        return START_STICKY;

    }


}
