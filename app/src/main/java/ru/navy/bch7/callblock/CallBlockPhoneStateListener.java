package ru.navy.bch7.callblock;

import android.content.Context;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;

public class CallBlockPhoneStateListener extends PhoneStateListener {
    final CallBlockService callBlockService;

    private Context mainContext;

    public CallBlockPhoneStateListener(CallBlockService callblockserivce, Context context)
    {
        callBlockService = callblockserivce;
        mainContext = context;


    }

    public final void onCallStateChanged(int state, String incomingNumber){
        Log.d(CallBlockService.TAG_CALLBLOCK, "CallBlockPhoneStateListener::onCallStateChanged");
        super.onCallStateChanged(state, incomingNumber);
        Log.d(CallBlockService.TAG_CALLBLOCK, "phone state changed = " + state);


        if (state == TelephonyManager.CALL_STATE_RINGING) {

                try
                {
                    TelephonyManager telephonyManager = (TelephonyManager) mainContext.getSystemService(Context.TELEPHONY_SERVICE);
                    Class<?> clazz = Class.forName(telephonyManager.getClass().getName());
                    Method method = clazz.getDeclaredMethod("getITelephony");
                    method.setAccessible(true);
                    ITelephony telephonyService = (ITelephony) method.invoke(telephonyManager);

                    Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(incomingNumber));
                    Cursor cursor = mainContext.getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

                    if (cursor != null && cursor.moveToFirst()) {
                        String name = cursor.getString(0);
                        cursor.close();
                    } else {
                        Log.d(CallBlockService.TAG_CALLBLOCK, "not in contacts, terminating call");
                        telephonyService.endCall();
                    }
                }
                catch (Exception ex)
                {
                    Log.d(CallBlockService.TAG_CALLBLOCK,
                            "could not end call"
                                    + ex);
                }


        }

    }
}
