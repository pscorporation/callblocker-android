package ru.navy.bch7.callblock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class CallBlockBootReceiver extends BroadcastReceiver {
    public CallBlockBootReceiver()
    {
    }

    public void onReceive(Context context, Intent intent)
    {
        Intent callBlockService = new Intent(context, CallBlockService.class);
        callBlockService.putExtras(intent);
        context.startService(callBlockService);
    }
}
