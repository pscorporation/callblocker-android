package ru.navy.bch7.callblock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;

public class CallBlockCallReceiver extends BroadcastReceiver {
    public CallBlockCallReceiver()
    {
    }



    public void onReceive(Context context, Intent intent)
    {
        Log.d(CallBlockService.TAG_CALLBLOCK, "CallBlockReceiver::onReceive");

        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

        if (state == null) {
            return;
        }

        if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)&& !CallBlockService.IsCommandStarted()){
                Log.d(CallBlockService.TAG_CALLBLOCK, "CallBlockReceiver::onReceive::RINGING");
                Log.d(CallBlockService.TAG_CALLBLOCK, "CallBlockReceiver::onReceive::startService");

            Intent callBlockService = new Intent(context, CallBlockService.class);
            callBlockService.putExtras(intent);
            context.startService(callBlockService);



        }


        /*if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_RINGING) && !CallBlockService.IsCommandStarted()) {
            Log.d(CallBlockService.TAG_CALLBLOCK, "CallBlockReceiver::onReceive::startService");



            context.startService(new Intent(context, CallBlockService.class));
        }*/

    }
}
